package Ejercicio_1;
public class Busqueda_Binaria {
    public static void main(String[] args) {
          // Arreglo apellidos
        String[] arreglo = { "Hernandez", "Crisanto", "Gregorio", "Duran", "Cahuich", "Ake"};
         // Arreglo nombres
         String[] nom = {"Brayan","Rafael","Julio","Cesar","Darlin","Edey"};
         //Arreglo numero telefonico
         String [] num= {"66666","55555","44444","33333","22222","11111"};
         
        String busqueda = "Gregorio";
        int indiceDelElementoBuscado = busquedaBinariaConWhile(arreglo, busqueda);
        System.out.println("[Con ciclo While] -- El elemento buscado ("
        + busqueda
        + ") se encuentra en el index "
        + indiceDelElementoBuscado);
        System.out.println("----------------- Información del directorio telefónico-------------------------");
        System.out.println("Apellido: "+busqueda+"- Nombre: "+nom[indiceDelElementoBuscado]+"- Numero: "+num[indiceDelElementoBuscado]);
    }
    
    public static int busquedaBinariaConWhile(String[] arreglo, String busqueda) {
    
    int izquierda = 0, derecha = arreglo.length - 1;
 
    while (izquierda <= derecha) {
        // Calculamos las mitades...
        int indiceDelElementoDelMedio = (int) Math.floor((izquierda + derecha) / 2);
        String elementoDelMedio = arreglo[indiceDelElementoDelMedio];
 
        
        // Primero vamos a comparar y ver si el resultado es negativo, positivo o 0
        int resultadoDeLaComparacion = busqueda.compareTo(elementoDelMedio);
 
        // Si el resultado de la comparación es 0, significa que ambos elementos son iguales
        // y por lo tanto quiere decir que hemos encontrado la búsqueda
        if (resultadoDeLaComparacion == 0) {
            return indiceDelElementoDelMedio;
        }
 
 
        // Si no, entonces vemos si está a la izquierda o derecha
 
        if (resultadoDeLaComparacion < 0) {
            derecha = indiceDelElementoDelMedio - 1;
        } else {
            izquierda = indiceDelElementoDelMedio + 1;
        }
    }
    // Si no se rompió el ciclo ni se regresó el índice, entonces el elemento no
    // existe
    return -1;
}
}
  

