package Ejercicio_2;

import java.util.Scanner;

public class Busqueda_Binaria {
    public static void main(String[] args) {
      String[] autor = {"bell hooks","Andreu Navarra","Matt Miller","Fernando Reimers"};
      String[] titulo = {"Enseñar a transgredir","Menos tech y más Platón ","Usa las TIC como un pirata","Educación global para mejorar el mundo"};
      //String[] arreglo = { "Chris", "Claire", "Django", "John", "Leon", "Morty", "Rick", "Saul", "Tuco", "Walter" };
      String [] asignatura= { "etica", "filosofia", "informatica", "orientacion escolar"};
      Scanner teclado = new Scanner(System.in);
      String busqueda;
      System.out.println("Ingrese la asignatura a buscar: ");
      busqueda = teclado.nextLine();
        int indiceDelElementoBuscado = busquedaBinariaConWhile(asignatura, busqueda);
        System.out.println("El elemento buscado ("
        + busqueda
        + ") se encuentra en el lugar "
        + indiceDelElementoBuscado);
        System.out.println("-----------------Libros existentes-------------------------");
        System.out.println("Asignatura: "+ asignatura[indiceDelElementoBuscado]);
        System.out.println("• Titulo: "+ titulo[indiceDelElementoBuscado]);
        System.out.println("• Autor: "+ autor[indiceDelElementoBuscado]);
    }
    public static int busquedaBinariaConWhile(String[] asignatura, String busqueda) {
    
    int izquierda = 0, derecha = asignatura.length - 1;
 
    while (izquierda <= derecha) {
        // Calculamos las mitades...
        int indiceDelElementoDelMedio = (int) Math.floor((izquierda + derecha) / 2);
        String elementoDelMedio = asignatura[indiceDelElementoDelMedio];
 
        
        // Primero vamos a comparar y ver si el resultado es negativo, positivo o 0
        int resultadoDeLaComparacion = busqueda.compareTo(elementoDelMedio);
 
        // Si el resultado de la comparación es 0, significa que ambos elementos son iguales
        // y por lo tanto quiere decir que hemos encontrado la búsqueda
        if (resultadoDeLaComparacion == 0) {
            return indiceDelElementoDelMedio;
        }
 
 
        // Si no, entonces vemos si está a la izquierda o derecha
 
        if (resultadoDeLaComparacion < 0) {
            derecha = indiceDelElementoDelMedio - 1;
        } else {
            izquierda = indiceDelElementoDelMedio + 1;
        }
    }
    // Si no se rompió el ciclo ni se regresó el índice, entonces el elemento no
    // existe
    return -1;
}
}
