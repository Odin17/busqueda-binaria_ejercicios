package Ejercicio_3;

import java.util.Scanner;

public class Busqueda_Binaria {
public static void main(String[] args) {
        int num_fac[] = {150,400,500,800};  //array con los números deseados
        String [] nom_cliente = {"Rafael","Julio","Darlin","Rodrigo"};
        String [] fecha = {"12/05/2021","02/03/2020","12/11/2019","19/09/2020"};
        int [] valor_fac = {1200,4500,20000,600};
         Scanner sc = new Scanner(System.in); 
         System.out.println("Digiite el numero de la factura a buscar: ");
        int key;  //Número que se quiere encontrar en el array
        key = sc.nextInt(); //leer un entero
        int last=num_fac.length-1;  //La última posición del array
        int result = binarySearch(num_fac,0,last,key);  //A la función se le pasa el array, un cero, la última posición del array y el numero buscado
        if (result == -1){  //si el resultado es -1 --> NO SE HA ENCONTRADO
            System.out.println("Element is not found!"); 
        }else  {
            System.out.println("Se encontro la factura solicitada en la posicion : "+result); 
        }
            System.out.println("-------------------Informacion completa del cliente-------------------");
            System.out.println("factura solicitada: "+key);
            System.out.println("• Nombre: "+nom_cliente[result]);
            System.out.println("• Fecha: "+fecha[result]);
            System.out.println("• valor de la factura: $ "+valor_fac[result]);
    }
     public static int binarySearch(int arr[], int first, int last, int key){  
        if (last>=first){ //si aún no se ha llegado a la primera posicion del array... 
            int mid = first + (last - first)/2;  //La posición media del array
            if (arr[mid] == key){  
            return mid;  //lo ha encontrado
            }  
            if (arr[mid] > key){  
            return binarySearch(arr, first, mid-1, key);//Buscará en el array izquierdo (menores que mid)
            }else{  
            return binarySearch(arr, mid+1, last, key);//Buscará en el array derecho (mayores que mid)  
            }  
        }  
        return -1;  
    }  
}
